package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class checkDeviceAndGet {
    sqlUpdateAndEntry sqlupdateandentry= new sqlUpdateAndEntry();
    
    public void checkDevicesAndProcess(ResultSet rsEvent, List<String> devFullNameAlertGen, String deviceTypeRgx, String deviceTyp, String errcode, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException {
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        List<String>  devFullNameAlertGenFinal=new ArrayList<>();
        //for update
        boolean matchUpdate;
        if((deviceTypeRgx.contentEquals("Inverter") || deviceTypeRgx.contentEquals("SMB")) && radiation>minPoaPlant){
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                matchUpdate=false;
                //param level alert being close here because of matching devcietype and alertgen zero, so added error code upper
                if (rsEvent.getString("device_type").contentEquals(deviceTyp) && rsEvent.getString("error_code").contentEquals(errcode)){
                    String eventId=rsEvent.getString("id");
                    String plantID=rsEvent.getString("plant_id");
                    String deviceID=rsEvent.getString("device_id");
                    String deviceName=rsEvent.getString("device_name");
                    String errorCode=rsEvent.getString("error_code");

                    for(String dev:devFullNameAlertGen){
                        if((dev).contains(plantID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                            matchUpdate=true;
                            break;
                        }
                    }
                    if(!matchUpdate){
                        sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                    }
                }
            }
        }
        else{
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                matchUpdate=false;
                //param level alert being close here because of matching devcietype and alertgen zero, so added error code upper
                if (rsEvent.getString("device_type").contentEquals(deviceTyp) && rsEvent.getString("error_code").contentEquals(errcode)){
                    String eventId=rsEvent.getString("id");
                    String plantID=rsEvent.getString("plant_id");
                    String deviceID=rsEvent.getString("device_id");
                    String deviceName=rsEvent.getString("device_name");
                    String errorCode=rsEvent.getString("error_code");

                    for(String dev:devFullNameAlertGen){
                        if((dev).contains(plantID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                            matchUpdate=true;
                            break;
                        }
                    }
                    if(!matchUpdate){
                        sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                    }
                }
            }
        }
        
        //for entry
        boolean matchEntry;
        for(String devDown:devFullNameAlertGen){
            String[] record= devDown.split(";");
            matchEntry=false;
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                if (rsEvent.getString("device_type").contentEquals(deviceTyp) && rsEvent.getString("error_code").contentEquals(errcode)){
                    String plantID=rsEvent.getString("plant_id");
                    String deviceID=rsEvent.getString("device_id");
                    String deviceName=rsEvent.getString("device_name");
                    String errorCode=rsEvent.getString("error_code");
                    //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                    if((plantID.contentEquals(record[0].split("_")[0])) && (deviceID.contentEquals(record[0].split("_")[2])) && (deviceName.contentEquals(record[1]))) {
                        matchEntry=true;
                        break;
                    }
                }
            }
            if(!matchEntry){
                if(deviceTypeRgx.contentEquals("Inverter") || deviceTypeRgx.contentEquals("SMB")){
                    sqlupdateandentry.sqlEveEntry1(record[1], record[2], record[0].split("_")[0], record[0].split("_")[2], deviceTyp, datetime, todayD, errcode, conn);
                }
                else{
                    sqlupdateandentry.sqlEveEntry(record[1], record[0].split("_")[0], record[0].split("_")[2], deviceTyp, datetime, todayD, errcode, conn);
                }
            }
        }
    }
}

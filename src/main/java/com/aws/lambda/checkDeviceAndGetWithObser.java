package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class checkDeviceAndGetWithObser {
    sqlUpdateAndEntry sqlupdateandentry= new sqlUpdateAndEntry();
    
    public void checkDevicesAndProcess(List<String> devFullNameAlertGen, String deviceTypeRgx, String devType, String errCode, float radiation, int minPoaPlant, int obserLimit, String siteID, String siteZone, Connection conn) throws SQLException, ParseException{
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));

        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String eventDetTempQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp where plant_id=%s and error_code=%s;", siteID, errCode);
        String eventDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and error_code=%s and end_date is null", siteID, errCode);
        ResultSet rsEventTemp= stmt1.executeQuery(eventDetTempQ);
        ResultSet rsEvent= stmt2.executeQuery(eventDetQ);
        
        String eventId="";
        String plantID="";
        String deviceID="";
        String deviceName="";
        String errorCode="";
        String startDateTime="";
        int actCount=0;
        boolean matchUpdate=false;
        boolean matchUpdateTemp=false;
        //radiation check is required here for alert close
        if(deviceTypeRgx.contentEquals("SMB") && radiation>minPoaPlant){
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                matchUpdate=false;
                //param level alert being close here because of matching devcietype and alertgen zero, so added error code upper
                if (rsEvent.getString("device_type").contentEquals(devType) && rsEvent.getString("error_code").contentEquals(errCode)){
                    eventId=rsEvent.getString("id");
                    plantID=rsEvent.getString("plant_id");
                    deviceID=rsEvent.getString("device_id");
                    deviceName=rsEvent.getString("device_name");
                    errorCode=rsEvent.getString("error_code");
                    for(String dev:devFullNameAlertGen){
                        if((dev).contains(plantID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                            matchUpdate=true;
                            break;
                        }
                    }
                    if(!matchUpdate){
                        sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                    }
                }
            }
            rsEventTemp.beforeFirst();
            while(rsEventTemp.next()){
                matchUpdateTemp=false;
                //param level alert being close here because of matching devcietype and alertgen zero, so added error code upper
                if (rsEventTemp.getString("device_type").contentEquals(devType) && rsEventTemp.getString("error_code").contentEquals(errCode)){
                    eventId=rsEventTemp.getString("id");
                    plantID=rsEventTemp.getString("plant_id");
                    deviceID=rsEventTemp.getString("device_id");
                    deviceName=rsEventTemp.getString("device_name");
                    errorCode=rsEventTemp.getString("error_code");
                    for(String dev:devFullNameAlertGen){
                        if((dev).contains(plantID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                            matchUpdateTemp=true;
                            break;
                        }
                    }
                    if(!matchUpdateTemp){
                        sqlupdateandentry.sqlEveDelete1(eventId, conn);
                    }
                }
            }
        }
        
        //for entry
        boolean matchEntry;
        boolean matchEntryTemp;
        for(String devDown:devFullNameAlertGen){
            String[] record= devDown.split(";");
            matchEntry=false;
            matchEntryTemp=false;
            rsEvent.beforeFirst();
            //check in main if entry available
            while(rsEvent.next()){
                if (rsEvent.getString("device_type").contentEquals(devType) && rsEvent.getString("error_code").contentEquals(errCode)){
                    plantID=rsEvent.getString("plant_id");
                    deviceID=rsEvent.getString("device_id");
                    deviceName=rsEvent.getString("device_name");
                    errorCode=rsEvent.getString("error_code");
                    //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                    if((plantID.contentEquals(record[0].split("_")[0])) && (deviceID.contentEquals(record[0].split("_")[2])) && (deviceName.contentEquals(record[1]))) {
                        matchEntry=true;
                        break;
                    }
                }
            }
            if(!matchEntry){
                rsEventTemp.beforeFirst();
                while(rsEventTemp.next()){
                    if (rsEventTemp.getString("device_type").contentEquals(devType) && rsEventTemp.getString("error_code").contentEquals(errCode)){
                        plantID=rsEventTemp.getString("plant_id");
                        deviceID=rsEventTemp.getString("device_id");
                        deviceName=rsEventTemp.getString("device_name");
                        errorCode=rsEventTemp.getString("error_code");
                        actCount=Integer.parseInt(rsEventTemp.getString("act_count"));
                        eventId=rsEventTemp.getString("id");
                        //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                        if((plantID.contentEquals(record[0].split("_")[0])) && (deviceID.contentEquals(record[0].split("_")[2])) && (deviceName.contentEquals(record[1]))) {
                            matchEntryTemp=true;
                            break;
                        }
                    }
                }
            }
            if(matchEntryTemp){
                //update it
                if(actCount<obserLimit){
                    sqlupdateandentry.sqlEveUpdate2(actCount, eventId, conn);
                }
                else if(actCount==obserLimit){
                    startDateTime=rsEventTemp.getString("date_time");
                    sqlupdateandentry.sqlEveEntry1(record[1], record[2], record[0].split("_")[0], record[0].split("_")[2], devType, startDateTime, new SimpleDateFormat("ddMMyyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDateTime)), errCode, conn);
                    sqlupdateandentry.sqlEveDelete1(eventId, conn);
                }
            }
            if(!matchEntry && !matchEntryTemp){
                sqlupdateandentry.sqlEveEntry2(record[1], record[2], record[0].split("_")[0], record[0].split("_")[2], devType, datetime, todayD, errCode, conn);
            }
        }
        
        
    }
}
package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class checkParamAndGet {
    sqlUpdateAndEntry sqlupdateandentry= new sqlUpdateAndEntry();
    
    public void checkDevicesAndProcess(ResultSet rsEvent, List<String> devFullNameAlertGen, String deviceTypeRgx, String deviceTyp, List<String> genErrCode, String siteID, String siteZone, Connection conn) throws SQLException {
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        List<String>  devFullNameAlertGenFinal=new ArrayList<>();
        boolean matchUpdate;
        //for update
        rsEvent.beforeFirst();
        while(rsEvent.next()){
            matchUpdate=false;
            if (rsEvent.getString("device_type").contentEquals(deviceTyp) && genErrCode.contains(rsEvent.getString("error_code"))){
                String eventId=rsEvent.getString("id");
                String plantID=rsEvent.getString("plant_id");
                String deviceID=rsEvent.getString("device_id");
                String deviceName=rsEvent.getString("device_name");
                String errorCode=rsEvent.getString("error_code");
                
                for(int i=0; i<devFullNameAlertGen.size();i++){
                    if((devFullNameAlertGen.get(i)+";"+genErrCode.get(i)).contentEquals(plantID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName+";"+errorCode)){
                        matchUpdate=true;
                        break;
                    }
                }
                if(!matchUpdate){
                    sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                }
            }
        }
        //for entry
        boolean matchEntry;
        for(int i=0; i<devFullNameAlertGen.size();i++){
            String[] record= devFullNameAlertGen.get(i).split(";");
            matchEntry=false;
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                if (rsEvent.getString("device_type").contentEquals(deviceTyp) && genErrCode.contains(rsEvent.getString("error_code"))){
                    String plantID=rsEvent.getString("plant_id");
                    String deviceID=rsEvent.getString("device_id");
                    String deviceName=rsEvent.getString("device_name");
                    String errorCode=rsEvent.getString("error_code");
                    //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                    if((plantID.contentEquals(record[0].split("_")[0])) && (deviceID.contentEquals(record[0].split("_")[2])) && (deviceName.contentEquals(record[1])) && (errorCode.contentEquals(genErrCode.get(i)))) {
                        matchEntry=true;
                        break;
                    }
                }
            }

            if(!matchEntry){
                sqlupdateandentry.sqlEveEntry(record[1], record[0].split("_")[0], record[0].split("_")[2], deviceTyp, datetime, todayD, genErrCode.get(i), conn);
            }
        }
        
    }
    
}

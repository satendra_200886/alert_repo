package com.aws.lambda;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.bson.Document;


public class dataProcess {
    mongoDataGet mongodataget=new mongoDataGet();
    nonCommunicationAlert noncommunicationalert=new nonCommunicationAlert();
    checkDeviceAndGet checkdeviceandget=new checkDeviceAndGet();
    invParamAlert invparamalert=new invParamAlert();
    smbParamAlert1 smbparamalert=new smbParamAlert1();
    annParamAlert annparamalert=new annParamAlert();
    vcbParamAlert vcbparamalert=new vcbParamAlert();
    powerActExpDev poweractexpdev=new powerActExpDev();
    wmsParamAlert wmsparamalert= new wmsParamAlert();
    trackParamAlert trackparamalert=new trackParamAlert();
    getData getdata =new getData();
    sqlUpdateAndEntry sqlupdateandentry=new sqlUpdateAndEntry();
    
    public void deviceProcess(String sitesI, MongoClient mongoclient, Connection conn) throws SQLException, NullPointerException, ParseException{
        String[] records=sitesI.split(";");
        String siteName=records[0];
        String siteID=records[1];
        String siteDB=records[2];
        String siteZone=records[3];

        MongoDatabase mongodatabase=null;
        MongoCollection<Document> mongocollection=null;
        try{
            mongodatabase = mongoclient.getDatabase(siteDB);
            mongocollection= mongodatabase.getCollection("payloads");
        }
        catch (Exception ex) {
            System.out.println("Catch Dataprocess mongoconnection "+siteName);
        }

        Instant timeInterval=ZonedDateTime.now(ZoneOffset.UTC).minusMinutes(4).toInstant();
        
        
        //all open alerts from this plant
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        String eventInvDet=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and (end_date=0 or end_date is null) order by error_code ASC", siteID);
        String plantInfoQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_plant_info where id=%s;", siteID);  
        ResultSet rsEvent= stmt1.executeQuery(eventInvDet);
        ResultSet plantInfoRs=stmt2.executeQuery(plantInfoQ);
        ResultSet deviceRs;
        
        String plantDcCapacity="0";
        int minPoaPlant=10;
        try{
            plantInfoRs.next();
            String valueHolderS=plantInfoRs.getString("plant_dc_capacity");
            if( valueHolderS!=null){
                plantDcCapacity= valueHolderS;
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in dataprocess dccapacity "+siteID);
        }
        try{
            String valueHolderS=plantInfoRs.getString("minpoa_gen");
            if( valueHolderS!=null){
                minPoaPlant= Integer.parseInt(valueHolderS);
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in dataprocess minpoagen"+siteID);
        }
        
        boolean plantDownStatus=true;
        try{
            plantDownStatus=mongoPlantLtstData(rsEvent, timeInterval, plantDcCapacity, siteID, siteZone, mongocollection, conn);
        }
        catch (Exception ex) {
            System.out.println("Catch in plant status check return");
        }
        if(plantDownStatus){
            return;
        }
        
        //to get wms parameter
        List<Float> list= irrModTempActPow(timeInterval,siteID,mongocollection,siteZone,conn);
        float actPow= list.get(0);
        float radiation= list.get(1);
        float modTemp= list.get(2);
        
        List<String> devListMongo=new ArrayList<>();
        List<Document> docList=new ArrayList<>();
        List<String> devFullName= new ArrayList<>();
        List<String> invIdCap= new ArrayList<>();
        String deviceTypeRgx="";
        //for remove smb which inv down
        List<String> invIDListAlert=new ArrayList<>();

        //for inverter nonocommunication generate alarm which are not in doclist, and other method on 
        deviceTypeRgx="Inverter";
        try{
            String invDevDetailQ=String.format("SELECT inverter_id, inverter_name, inverter_dc_capacity FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s order by inverter_id ASC;", siteID);  
            deviceRs=stmt3.executeQuery(invDevDetailQ);
            String invID;
            String invCap;
            while(deviceRs.next()){
                invID=deviceRs.getString("inverter_id");
                invCap=deviceRs.getString("inverter_dc_capacity");
                devFullName.add(siteID+"_Inverter_"+deviceRs.getString("inverter_id")+";"+deviceRs.getString("inverter_name")+";"+deviceRs.getString("inverter_dc_capacity"));
            }
            if(devFullName.size()>0){
                mongoListsReturn invmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(invmongodevRgxList.getdevList());
                docList=new ArrayList<>(invmongodevRgxList.getdocList());
                invIDListAlert.addAll(noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn));
                if(docList.size()>0 && radiation>minPoaPlant){
                    invIDListAlert.addAll(invparamalert.invActPowAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn)); 
                    if(siteID.contentEquals("11") || siteID.contentEquals("137") || siteID.contentEquals("41") || siteID.contentEquals("42")){
                        invparamalert.invStringAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                    }
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in inverter device dataprocess "+siteID);
        }
        
        //for SMB
        try{
            deviceTypeRgx="SMB";
            String smbDevDetailQ;
            if(invIDListAlert.size()>0){
                smbDevDetailQ=String.format("SELECT SCB_id, SCB_name, scb_dc_capacity FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s and Inverter_id not in (%s);", siteID, String.join(",", invIDListAlert));  
            }
            else{
                smbDevDetailQ=String.format("SELECT SCB_id, SCB_name, scb_dc_capacity FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s;", siteID);
            }
            deviceRs=stmt3.executeQuery(smbDevDetailQ);
            //List<String> invFullD evName is string of device and thier name fully indetification
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_SMB_"+deviceRs.getString("SCB_id")+";"+deviceRs.getString("SCB_name")+";"+deviceRs.getString("scb_dc_capacity"));
            }
            if(devFullName.size()>0){
                mongoListsReturn smbmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(smbmongodevRgxList.getdevList());
                docList=new ArrayList<>(smbmongodevRgxList.getdocList());
                noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                if(docList.size()>0 && radiation>minPoaPlant){
                    smbparamalert.smbFaultAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn); 
                    smbparamalert.smbStringAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in SMB device dataprocess "+siteID);
        }

        //for Annunciator
        try{
            deviceTypeRgx="Annunciator";
            String annDevDetailQ=String.format("SELECT Annunciator_id, Annunciator_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_Annunciator_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(annDevDetailQ);
            //List<String> invFullDevName is string of device and thier name fully indetification
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_Annunciator_"+deviceRs.getString("Annunciator_id")+";"+deviceRs.getString("Annunciator_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn annmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(annmongodevRgxList.getdevList());
                docList=new ArrayList<>(annmongodevRgxList.getdocList());
                if(docList.size()>0){
                    annparamalert.annParamAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn); 
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in Annunciator device dataprocess "+siteID);
        }

        //for vcb
        try{
            deviceTypeRgx="VCB";
            String vcbDevDetailQ=String.format("SELECT vcb_id, vcb_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_VCB_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(vcbDevDetailQ);
            //List<String> invFullDevName is string of device and thier name fully indetification
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_VCB_"+deviceRs.getString("vcb_id")+";"+deviceRs.getString("vcb_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn vcbmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(vcbmongodevRgxList.getdevList());
                docList=new ArrayList<>(vcbmongodevRgxList.getdocList());
                if(docList.size()>0){
                    vcbparamalert.vcbParamAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn); 
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in VCB device dataprocess "+siteID);
        }

        //for wms
        try{
            deviceTypeRgx="WMS";
            String wmsDevDetailQ=String.format("SELECT wms_id, wms_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(wmsDevDetailQ);
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_WMS_"+deviceRs.getString("wms_id")+";"+deviceRs.getString("wms_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn wmsmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(wmsmongodevRgxList.getdevList());
                docList=new ArrayList<>(wmsmongodevRgxList.getdocList());
                noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                if(docList.size()>0){
                    wmsparamalert.wmsSensorAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn);
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in WMS device dataprocess "+siteID);
        }
        
        //for mfm
        try{
            deviceTypeRgx="MFM";
            String mfmDevDetailQ=String.format("SELECT M_ID, M_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(mfmDevDetailQ);
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_MFM_"+deviceRs.getString("M_ID")+";"+deviceRs.getString("M_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn mfmmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(mfmmongodevRgxList.getdevList());
                docList=new ArrayList<>(mfmmongodevRgxList.getdocList());
                noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in MFM dataprocess "+siteID);
        }
        
        //for UPS
        try{
            deviceTypeRgx="UPS";
            String upsDevDetailQ=String.format("SELECT UPS_id, UPS_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_UPS_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(upsDevDetailQ);
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_UPS_"+deviceRs.getString("UPS_id")+";"+deviceRs.getString("UPS_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn mfmmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(mfmmongodevRgxList.getdevList());
                docList=new ArrayList<>(mfmmongodevRgxList.getdocList());
                noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in UPS dataprocess "+siteID);
        }
        
        //for Tracker
        try{
            deviceTypeRgx="Tracker";
            String trackDevDetailQ=String.format("SELECT Tracker_id, Tracker_NCU_name FROM swlflexi_CMSswlawsReMACS.Flexi_solar_Tracker_list where plant_id=%s;", siteID);  
            deviceRs=stmt3.executeQuery(trackDevDetailQ);
            devFullName.clear();
            while(deviceRs.next()){
                devFullName.add(siteID+"_Tracker_"+deviceRs.getString("Tracker_id")+";"+deviceRs.getString("Tracker_NCU_name"));
            }
            if(devFullName.size()>0){
                mongoListsReturn trackmongodevRgxList=mongodataget.mongoDeviceLtstData(deviceTypeRgx, timeInterval, siteID, mongocollection, conn);
                devListMongo=new ArrayList<>(trackmongodevRgxList.getdevList());
                docList=new ArrayList<>(trackmongodevRgxList.getdocList());
                noncommunicationalert.commAlert(devListMongo, docList, rsEvent, devFullName, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                if(docList.size()>0){
                    trackparamalert.trackFaultAlert(docList, devFullName, rsEvent, deviceTypeRgx, radiation, minPoaPlant, siteID, siteZone, conn);
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch in Tracker device dataprocess "+siteID);
        }
        
        //poweractexpdev.powerDevAlert(rsEvent, actPow, radiation, modTemp, 2, siteID,siteZone, conn);
        
        //release resources
        try{
            rsEvent.close();
            stmt1.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }   
    
    
    public boolean mongoPlantLtstData(ResultSet rsEvent, Instant timeInterval, String plantDcCapacity, String siteID, String siteZone, MongoCollection mongocollection, Connection conn) throws SQLException, ParseException{
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String datetime=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone(siteZone));
        long bdMinute=0;
        
        Statement stmt1= conn.createStatement();
        List<String> devListMongo;
        List<Document> docList;
        String device="Inverter";
        mongoListsReturn mongodevRgxList=mongodataget.mongoDeviceLtstData(device, timeInterval, siteID, mongocollection, conn);
        devListMongo=new ArrayList<>(mongodevRgxList.getdevList());
        docList=new ArrayList<>(mongodevRgxList.getdocList());

        if(devListMongo==null || devListMongo.isEmpty()){
            //sql event check and update for plant comm failure
            boolean matchEntry=true;
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                String plantID=rsEvent.getString("plant_id");
                String deviceID=rsEvent.getString("device_id");
                String deviceName=rsEvent.getString("device_name");
                String deviceType=rsEvent.getString("device_type");
                String errorCode=rsEvent.getString("error_code");
                if(plantID.contentEquals(siteID) && deviceID.contentEquals(siteID) && deviceName.contentEquals("Plant") && deviceType.contentEquals("Plant") && errorCode.contentEquals("1000")){
                    matchEntry=false;
                    break;
                }
            }
            if(matchEntry){
                sqlupdateandentry.sqlEveEntry1("Plant", plantDcCapacity, siteID, siteID, "Plant", datetime, todayD, "1000", conn);
            }
            //stop excution other statement
            return true;
        }
        //close the plant comm event if data getting from mongo available.
        else{
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                String eventId=rsEvent.getString("id");
                String plantID=rsEvent.getString("plant_id");
                String deviceID=rsEvent.getString("device_id");
                String deviceName=rsEvent.getString("device_name");
                String deviceType=rsEvent.getString("device_type");
                String errorCode=rsEvent.getString("error_code");
                if(plantID.contentEquals(siteID) && deviceID.contentEquals(siteID) && deviceName.contentEquals("Plant") && deviceType.contentEquals("Plant") && errorCode.contentEquals("1000")){
                    String devDetailQ=String.format("SELECT COUNT(id) FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s", siteID);  
                    ResultSet invCountRs=stmt1.executeQuery(devDetailQ);
                    int devCount=0;
                    while(invCountRs.next()){
                        devCount=Integer.parseInt(invCountRs.getString("COUNT(id)"));
                    }

                    bdMinute=(dateFormat.parse(datetime).getTime()-dateFormat.parse(rsEvent.getString("start_date")).getTime())/(1000*60);
                    
                    docList=getdata.mongoDevBtwData("Inverter", (ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(datetime).toInstant(), ZoneOffset.UTC).toInstant()), mongocollection, conn, siteID);
                    if(bdMinute<=5 || (docList.size()>((devCount*bdMinute)*.50))){
                        sqlupdateandentry.sqlEveDelete(eventId, conn);
                        break;
                    }
                    else if(docList.size()<((devCount*bdMinute)*.50)){
                        sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                        break;
                    }
                }
            }
        }
        
        try{
            stmt1.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        
        return false;
    }

    
    public List<Float> irrModTempActPow(Instant timeInterval,String siteID,MongoCollection mongocollection,String siteZone,Connection conn) throws SQLException{
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        List<Float> returnList= new ArrayList<>();
        List<String> wmsListCumm= new ArrayList<>();
        List<String> mfmListCumm= new ArrayList<>();
        float radiation= -1.5f;
        float modtemp= -1.5f;
        float actPow= -1.5f;
        Statement stmt1=conn.createStatement();
        Statement stmt2=conn.createStatement();
        Statement stmt3=conn.createStatement();
        String wmsDetailQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s and WMS_Cumulative=1;", siteID);  
        String mfmDetailQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s and MFM_Cumulative=1;", siteID);  
        String wmsSensorEventQ=String.format("SELECT * FROM Flexi_solar_events WHERE plant_id =%s AND (start_today=%s or end_today=%s or end_today is null) AND error_code IN (8000,8001) group by device_name;",siteID, todayD, todayD);
        ResultSet wmsDetailRs=stmt1.executeQuery(wmsDetailQ);
        ResultSet mfmDetailRs=stmt2.executeQuery(mfmDetailQ);
        ResultSet wmsSensorEventRs=stmt3.executeQuery(wmsSensorEventQ);
        
        while(wmsDetailRs.next()){
            wmsListCumm.add(wmsDetailRs.getString("plant_id")+"_WMS_"+wmsDetailRs.getString("wms_id"));
        }
        while(mfmDetailRs.next()){
            mfmListCumm.add(mfmDetailRs.getString("plant_id")+"_MFM_"+mfmDetailRs.getString("M_ID"));
        }
        
        boolean poaFail1=false;
        boolean poaFail2=false;
        boolean ghiFail1=false;
        boolean modTemFail1=false;
        boolean modTemFail2=false;
        boolean ambTemFail1=false;
        int r=0;
        int m=0;
        float rTot=0.0f;
        float mTot=0.0f;
        float valueHolderF=0.0f;
        Set keyset;
        
        for(String wms:wmsListCumm){
            mongoListsReturn wmsDevRgxList=mongodataget.mongoDeviceLtstData(wms, timeInterval, siteID, mongocollection, conn);
            List<Document> wmsDocList=new ArrayList<>(wmsDevRgxList.getdocList());
            poaFail1=false;
            poaFail2=false;
            ghiFail1=false;
            modTemFail1=false;
            modTemFail2=false;
            ambTemFail1=false;
            wmsSensorEventRs.beforeFirst();
            while(wmsSensorEventRs.next()){
                if((wms.split("_")[2]).contentEquals(wmsSensorEventRs.getString("device_id"))){
                    if(wmsSensorEventRs.getString("device_name").endsWith("POA-1")){
                        poaFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("POA-2")){
                        poaFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp1")){
                        modTemFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp2")){
                        modTemFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("GHI-1")){
                        ghiFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Amb-Temp")){
                        ambTemFail1=true;
                    }
                    break;
                }
            }
            
            for(Document doc:wmsDocList){
                if(wmsListCumm.contains(doc.getString("device"))){
                    keyset=doc.keySet();
                    try{
                        if(!poaFail1){
                            if(keyset.contains("w21")){
                                valueHolderF=Float.parseFloat(doc.get("w21").toString());
                                if(!Float.isNaN(valueHolderF) && valueHolderF>0){
                                    rTot=rTot+valueHolderF;
                                    r++;
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    try{
                        if(!poaFail2){
                            if(keyset.contains("w22")){
                                valueHolderF=Float.parseFloat(doc.get("w22").toString());
                                if(!Float.isNaN(valueHolderF) && valueHolderF>0){
                                    rTot=rTot+valueHolderF;
                                    r++;
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    try{
                        if(!modTemFail1){
                            if(keyset.contains("w10")){
                                valueHolderF=Float.parseFloat(doc.get("w10").toString());
                                if(!Float.isNaN(valueHolderF) && valueHolderF>0){
                                    mTot=mTot+valueHolderF;
                                    m++;
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    try{
                        if(!modTemFail2){
                            if(keyset.contains("w14")){
                                valueHolderF=Float.parseFloat(doc.get("w14").toString());
                                if(!Float.isNaN(valueHolderF) && valueHolderF>0){
                                    mTot=mTot+valueHolderF;
                                    m++;
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        
        if(r>0){
            radiation=rTot/r;
        }
        if(m>0){
            modtemp=mTot/m;
        }
        
        mongoListsReturn mfmdevRgxList=mongodataget.mongoDeviceLtstData("MFM", timeInterval, siteID, mongocollection, conn);
        List<Document> mfmDocList=new ArrayList<>(mfmdevRgxList.getdocList());
        for(Document doc:mfmDocList){
            if(mfmListCumm.contains(doc.getString("device"))){
                try{
                    valueHolderF=Float.parseFloat(doc.get("m32").toString());
                    if(valueHolderF>0){
                        actPow=actPow+Float.parseFloat(doc.get("m32").toString());
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        returnList.add(actPow);
        returnList.add(radiation);
        returnList.add(modtemp);
        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }

        return returnList;
    }
    
}
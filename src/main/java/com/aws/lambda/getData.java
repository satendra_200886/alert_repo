package com.aws.lambda;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Indexes.ascending;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.bson.Document;

public class getData {

    public List<Document> mongoDevBtwData(String devRgx, Instant dateTimeStart, Instant dateTimeStop, MongoCollection<Document> mongocollection, Connection conn, String siteID) throws SQLException, ParseException{
        List<Document> docList=new ArrayList<>();
        List<Document> docListFinal=new ArrayList<>();

        try{
            docList= (List<Document>) mongocollection.find(and(gte("ts", dateTimeStart), lt("ts", dateTimeStop), regex("device",devRgx))).sort(orderBy(ascending("ts"))).into(new ArrayList<>());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //to remove duplicate by ts
        Date ts=new Date(0);
        for(Document doc:docList){
            if(ts!=doc.getDate("ts")){
                docListFinal.add(doc);
            }
            ts=doc.getDate("ts");
        }

        return docListFinal;
    }
    
   
}


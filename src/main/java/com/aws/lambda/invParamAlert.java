
package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.bson.Document;

public class invParamAlert {
    checkDeviceAndGet checkdeviceandget= new checkDeviceAndGet();
    
    public List<String> invActPowAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        String errorCode="2001";
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<String> invAlertList= new ArrayList<>();
        List<Document> docListRem=new ArrayList<>();
        for(Document doc:docList){
            try{
                if(radiation>minPoaPlant && Float.parseFloat(doc.get("i15").toString())<=0.0){
                    invAlertList.add(doc.getString("device"));
                    docListRem.add(doc);
                }
            }
            catch (Exception ex) {
                System.out.println("Catch in adding tracker fault");
            }
        }

        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> invIDList=new ArrayList<>();
        for (String devMongo: invAlertList){
            for (String devSQL: devFullName){
                //for correct matching at once each
                if(devSQL.contains(devMongo+";")){
                    devFullNameAlertGen.add(devSQL);
                    invIDList.add(devMongo.split(";")[0].split("_")[2]);
                    break;
                }
            }
        }
        devFullName.removeAll(devFullNameAlertGen);
        docList.removeAll(docListRem);
        try{
           checkdeviceandget.checkDevicesAndProcess(rsEvent, devFullNameAlertGen, deviceTypeRgx, "Inverter", errorCode, radiation, minPoaPlant, siteID, siteZone, conn); 
        }
        catch (Exception ex) {
            System.out.println("Catch in inverter checkdeviceandget");
        }

        return invIDList;
    }
    
    
    public void invStringAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        //errorcode is not being use as we make it common as 3000
        Statement stmt1= conn.createStatement();
        String stringCapQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s;", siteID);  
        ResultSet stringCapRs=stmt1.executeQuery(stringCapQ);
        String dcCapacity="0";
        String[] record=new String[3];
        List<String> stringDcCap=new ArrayList<String>();
        float valuHolderF=0.0f;

        String[][] tagArray={{"i62","String-1","3000"}, {"i63","String-2","3001"}, {"i64","String-3","3002"}, {"i65","String-4","3003"}, {"i66","String-5","3004"}, {"i67","String-6","3005"}, {"i68","String-7","3006"}, {"i69","String-8","3007"}, {"i70","String-9","3008"}, {"i71","String-10","3009"}, {"i72","String-11","3010"}, 
            {"i73","String-12","3011"}, {"i74","String-13","3012"}, {"i75","String-14","3013"}, {"i76","String-15","3014"}, {"i77","String-16","3015"}, {"i78","String-17","3016"}, {"i79","String-18","3017"}, {"i80","String-19","3018"}, {"i81","String-20","3019"}, {"i82","String-21","3020"}, {"i83","String-22","3021"}, {"i84","String-23","3022"}, {"i85","String-24","3023"}};
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> genErrCode= new ArrayList<>();
        
        for(Document doc:docList){
            stringCapRs.beforeFirst();
            while(stringCapRs.next()){
                if(stringCapRs.getString("inverter_id").contentEquals(doc.getString("device").split("_")[2])){
                    stringDcCap.addAll(Arrays.asList(stringCapRs.getString("scb_dc_capacity").split(",")));
                }
            }
            Set<String> keySet=doc.keySet();
            for(String key :keySet){
                String tag=null;
                String error=null;
                String tagkey=null;
                for(int i=0; i<tagArray.length; i++){
                    if(key.contentEquals(tagArray[i][0])){
                        tagkey=tagArray[i][0];
                        tag=tagArray[i][1];
                        if(stringDcCap.size()>i){
                            dcCapacity=stringDcCap.get(i);
                        }
                        else{
                            dcCapacity="0";
                        }
                        error="3000";
                        break;
                    }
                }
                if(tagkey!=null){
                    try{
                        valuHolderF=Float.parseFloat(doc.get(key).toString());
                    }
                    catch(Exception ex){
                        System.out.println("Catch in smb string valuholder");
                    }
                }
                if(!Float.isNaN(valuHolderF)){
                    if(radiation>minPoaPlant && tagkey!=null && (valuHolderF<=0.0)){
                        for (String devSQL: devFullName){
                            if(devSQL.contains(doc.getString("device")+";")){
                                record=devSQL.split(";");
                                devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+dcCapacity);
                                genErrCode.add(error);
                                break;
                            }
                        }
                        //continue;
                    }
                }
            }
        }
        try{
            checkdeviceandget.checkDevicesAndProcess(rsEvent, devFullNameAlertGen, deviceTypeRgx, "String", "3000", radiation, minPoaPlant, siteID, siteZone, conn); 
            }
        catch (Exception ex) {
            System.out.println("Catch in smbstringalert checkdeviceandget");
        }

    }
    
}

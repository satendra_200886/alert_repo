
package com.aws.lambda;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

public class mongoDataGet {
    
    public mongoListsReturn mongoDeviceLtstData(String devRgx, Instant timeInterval, String siteID, MongoCollection mongocollection, Connection conn) throws SQLException{
        List<String> devList=new ArrayList<>();
        List<Document> docListLatest=new ArrayList<>();
        List<Document> docListFinal=new ArrayList<>();
        List<String> devListRemove=new ArrayList<>();
        List<Document> docListRemove=new ArrayList<>();
        
//        Statement stmt=conn.createStatement();
//        String devDetailQ=null;
//        if(devRgx.contains("WMS")){
//            devDetailQ=String.format("SELECT wms_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("Inverter")){
//            devDetailQ=String.format("SELECT inverter_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("SMB")){
//            devDetailQ=String.format("SELECT SCB_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("MFM")){
//            devDetailQ=String.format("SELECT M_ID FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("Annunciator")){
//            devDetailQ=String.format("SELECT Annunciator_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_Annunciator_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("VCB")){
//            devDetailQ=String.format("SELECT vcb_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_VCB_list where plant_id=%s", siteID);  
//        }

        
//        ResultSet devDetailRs=stmt.executeQuery(devDetailQ);
//        int devCount=0;
//        while(devDetailRs.next()){
//            devCount++;
//        }

        try{
            docListLatest= (List<Document>) mongocollection.find(and(gte("ts", timeInterval), regex("device",devRgx))).sort(orderBy(descending("ts"))).into(new ArrayList<>());
            //docListLatest= (List<Document>) mongocollection.find(and(gte("ts", dateTime), regex("device",devRgx))).sort(orderBy(descending("ts"))).limit(devCount).into(new ArrayList<>());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //to remove duplicate by device
        for(Document doc:docListLatest){
            if(!devList.contains(doc.get("device").toString())){
                docListFinal.add(doc);
                devList.add(doc.get("device").toString());
            }
        }
        //check all key value not should be zero
        if(!devRgx.contentEquals("Annunciator") && !devRgx.contentEquals("VCB")){
            int p;
            for (Document doc:docListFinal){
                p=0;
                for(String key:doc.keySet()){
                    if((doc.get(key).toString().contentEquals("0")) || (doc.get(key).toString().contentEquals("NaN"))){
                        p++;
                    }
                }
                if(p>=(doc.keySet().size()-7)){
                    devListRemove.add(doc.getString("device"));
                    docListRemove.add(doc);
                }
            }
        }
        devList.removeAll(devListRemove);
        docListFinal.removeAll(docListRemove);
        
        return new mongoListsReturn(devList, docListFinal);
    }
    
    
}

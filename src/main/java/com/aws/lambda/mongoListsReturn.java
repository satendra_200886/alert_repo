
package com.aws.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bson.Document;

public class mongoListsReturn {
    private final List<String> devList;
    private final List<Document> docList;

    public mongoListsReturn(List<String> devList, List<Document> docList) {
        this.devList = new ArrayList<>(devList);
        this.docList= new ArrayList<>(docList);
    }

    public List<String> getdevList() {
        return devList;
    }

    public List<Document> getdocList() {
        return docList;
    }
    
}

package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

public class nonCommunicationAlert {
    checkDeviceAndGet checkdeviceandget= new checkDeviceAndGet();
    
    public List<String> commAlert(List<String> devListMongo, List<Document> docList, ResultSet rsEvent, List<String> devFullName, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        //devices full name list which are available
        List<String> devFullNameAlertGen=new ArrayList<>();
        List<String> devFullNameAlertClose=new ArrayList<>();
        List<String> invIDList=new ArrayList<>();
        
        for (String devMongo: devListMongo){
            for (String devSQL: devFullName){
                //for correct matching at once each
                if(devSQL.contains(devMongo+";")){
                    devFullNameAlertClose.add(devSQL);
                    break;
                }
            }
        }
        
        devFullNameAlertGen=new ArrayList<>(devFullName);
        devFullNameAlertGen.removeAll(devFullNameAlertClose);

        try{
            checkdeviceandget.checkDevicesAndProcess(rsEvent, devFullNameAlertGen, deviceTypeRgx, deviceTypeRgx, "1000", radiation, minPoaPlant, siteID, siteZone, conn);
        }
        catch (Exception ex) {
            System.out.println("Catch in noncommunication checkdeviceandget "+deviceTypeRgx);
        }
        
        for(String d:devFullNameAlertGen){
            invIDList.add(d.split(";")[0].split("_")[2]);
        }

        return invIDList;
    }
}

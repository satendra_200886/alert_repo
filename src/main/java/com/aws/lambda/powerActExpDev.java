package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class powerActExpDev {
    sqlUpdateAndEntry sqlupdateandentry= new sqlUpdateAndEntry();
    
    public void powerDevAlert(ResultSet rsEvent, float actPow, float radition, float modTemp, int devLimit, String siteID, String siteZone, Connection conn) throws SQLException, ParseException{
        String errorcode="7000";
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat docFormat = new SimpleDateFormat("yyyy-MM-dd");
        String constantSQL=String.format("SELECT plant_dc_capacity, loss_factor_plant_level, module_temp_coefficient, DOC FROM swlflexi_CMSswlawsReMACS.Flexi_solar_plant_info where id=%s", siteID);  
        Statement stmt= conn.createStatement();
        ResultSet rs=stmt.executeQuery(constantSQL);
        
        rs.next();
        float dcCapacity= Float.parseFloat(rs.getString("plant_dc_capacity"));
        float lossFactor= Float.parseFloat(rs.getString("loss_factor_plant_level"));
        float modTempCoeff= Float.parseFloat(rs.getString("module_temp_coefficient"));
        Date doc= docFormat.parse(rs.getString("DOC"));
        
        long daysPassed= (dateFormat.parse(datetime).getTime() - doc.getTime())/(24 * 60 * 60 * 1000);
        
        float degradation= (float) ((daysPassed-365)*(0.7/36500));
        
        float expPower = dcCapacity*(1-degradation)*(1-lossFactor)*(1+(modTempCoeff*((modTemp+(radition/1000)*3)-25)))*(radition/1000);
        
        float dev=(actPow-expPower)/expPower;
        if(Math.abs(dev)>devLimit){
            boolean matchEntry=true;
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                String plantID=rsEvent.getString("plant_id");
                String deviceID=rsEvent.getString("device_id");
                String deviceName=rsEvent.getString("device_name");
                String deviceType=rsEvent.getString("device_type");
                String errorCode=rsEvent.getString("error_code");
                if(plantID.contentEquals(siteID) && deviceID.contentEquals(siteID) && deviceName.contentEquals("Plant") && deviceType.contentEquals("Plant") && errorCode.contentEquals(errorcode)){
                    matchEntry=false;
                    break;
                }
            }
            if(matchEntry){
                    sqlupdateandentry.sqlEveEntry("Plant", siteID, siteID, "PlantPowDev", datetime, todayD, errorcode, conn);
            }
        }
        //close the alert if it is not active
        else{
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                String eventId=rsEvent.getString("id");
                String plantID=rsEvent.getString("plant_id");
                String deviceID=rsEvent.getString("device_id");
                String deviceName=rsEvent.getString("device_name");
                String deviceType=rsEvent.getString("device_type");
                String errorCode=rsEvent.getString("error_code");
                String startDate=rsEvent.getString("start_date");
                DateFormat sqlDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                if(plantID.contentEquals(siteID) && deviceID.contentEquals(siteID) && deviceName.contentEquals("Plant") && deviceType.contentEquals("Plant") && errorCode.contentEquals(errorcode)){
                    long timeDiff= sqlDateFormat.parse(startDate).getTime()-dateFormat.parse(datetime).getTime();
                    if((timeDiff/(60*1000))>=15){
                        sqlupdateandentry.sqlEveUpdate(datetime, todayD, eventId, conn);
                    }
                    else{
                        sqlupdateandentry.sqlEveDelete(eventId, conn);
                    }
                }
            }
        }
    }
    
}

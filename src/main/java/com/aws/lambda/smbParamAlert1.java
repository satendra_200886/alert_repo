package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.bson.Document;

public class smbParamAlert1{
    checkDeviceAndGetWithObser checkdeviceandgetwithobser= new checkDeviceAndGetWithObser();
    
    public void smbFaultAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        String errorCode="3001";
        int obserLimit=9;
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String datetime=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        List<String> smbInvDcCurrList=new ArrayList<>();
        String smbInvDcCurrentQ=String.format("SELECT invdaily.Inverter_id,scblist.SCB_id,invdaily.DC_Current FROM Flexi_solar_inverters_daily invdaily LEFT JOIN Flexi_solar_SCB_list scblist "
                + "ON scblist.Inverter_id = invdaily.Inverter_id AND scblist.plant_id = invdaily.plant_id WHERE invdaily.Today = %s AND invdaily.plant_id = %s Order by scblist.SCB_id;",siteID,todayD);
        Statement stmt1= conn.createStatement();
        ResultSet smbInvDcCurrentRs=stmt1.executeQuery(smbInvDcCurrentQ);
        while(smbInvDcCurrentRs.next()){
            smbInvDcCurrList.add(smbInvDcCurrentRs.getString("SCB_id")+";"+smbInvDcCurrentRs.getString("DC_Current"));
        }
        
        List<String> invAlertList= new ArrayList<>();
        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> smbApiId = Arrays.asList("s11","s12","s13","s14","s15","s16","s17","s18","s19","s20","s21","s22","s23","s24","s25","s26","s27","s28","s29","s30","s31","s32","s33","s34");
        List<Document> docListRem=new ArrayList<>();
        int j, k;
        float smbTotCurr=0.0f;
        float valueHolderF=0.0f;
        String[] deviceArr=new String[3];
        float smbInvCurr=0.0f;
        for(Document doc:docList){
            deviceArr=doc.getString("device").split("_");
            //getiing smb inverter current
            for(String invcurr:smbInvDcCurrList){
                if(invcurr.split(";")[0].contentEquals(deviceArr[0])){
                    try{
                        smbInvCurr=Float.parseFloat(invcurr.split(";")[1]);
                    }
                    catch (Exception ex) {
                        System.out.println("Catch in smbfaultalertget invsmbcurrent");
                    }
                }
            }
            if(deviceArr[1].contentEquals("SMB") && smbInvCurr>1.0){
                j=0;
                k=0;
                smbTotCurr=0.0f;
                for(String key:doc.keySet()){
                    if(smbApiId.contains(key)){
                        try{
                            valueHolderF=Float.parseFloat(doc.get(key).toString());
                        }
                        catch (Exception ex) {
                            System.out.println("Catch in smbfaultalertget keyread");
                        }
                        if(!Float.isNaN(valueHolderF)){
                            smbTotCurr=smbTotCurr+valueHolderF;
                        }
                    }
                }
                if(smbTotCurr<1.0){
                    for (String devSQL: devFullName){
                        if(devSQL.contains(doc.getString("device")+";")){
                            devFullNameAlertGen.add(devSQL);
                            docListRem.add(doc);
                            break;
                        }
                    }
                }
            }
        }
        
        devFullName.removeAll(devFullNameAlertGen);
        docList.removeAll(docListRem);

        try{
            checkdeviceandgetwithobser.checkDevicesAndProcess(devFullNameAlertGen, deviceTypeRgx, "SMB", errorCode, radiation, minPoaPlant, obserLimit, siteID, siteZone, conn); 
        }
        catch (Exception ex) {
            System.out.println("Catch in smbfaultalert checkdeviceandget");
        }
    }
    
    
    public void smbStringAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        //errorcode is not being use as we make it common as 3000
        int obserLimit=29;
        Statement stmt1= conn.createStatement();
        String stringCapQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s;", siteID);  
        ResultSet stringCapRs=stmt1.executeQuery(stringCapQ);
        String dcCapacity="0";
        String[] record=new String[3];
        List<String> stringDcCap=new ArrayList<String>();
        float valuHolderF=0.0f;
        List<String> smbApiId = Arrays.asList("s11","s12","s13","s14","s15","s16","s17","s18","s19","s20","s21","s22","s23","s24","s25","s26","s27","s28","s29","s30","s31","s32","s33","s34");
        String[][] tagArray={{"s11","String-1","3000"}, {"s12","String-2","3001"}, {"s13","String-3","3002"}, {"s14","String-4","3003"}, {"s15","String-5","3004"}, {"s16","String-6","3005"}, {"s17","String-7","3006"}, {"s18","String-8","3007"}, {"s19","String-9","3008"}, {"s20","String-10","3009"}, {"s21","String-11","3010"}, 
            {"s22","String-12","3011"}, {"s23","String-13","3012"}, {"s24","String-14","3013"}, {"s25","String-15","3014"}, {"s26","String-16","3015"}, {"s27","String-17","3016"}, {"s28","String-18","3017"}, {"s29","String-19","3018"}, {"s30","String-20","3019"}, {"s31","String-21","3020"}, {"s32","String-22","3021"}, {"s33","String-23","3022"}, {"s34","String-24","3023"}};
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> devFullNameAlertClose= new ArrayList<>();
        List<String> genErrCode= new ArrayList<>();
        List<String> closeErrCode= new ArrayList<>();
        float smbTotCurr=0.0f;
        float valueHolderF=0.0f;
        
        for(Document doc:docList){
            stringCapRs.beforeFirst();
            while(stringCapRs.next()){
                if(stringCapRs.getString("SCB_id").contentEquals(doc.getString("device").split("_")[2])){
                    stringDcCap.addAll(Arrays.asList(stringCapRs.getString("string_dc_capacity").split(",")));
                }
            }
            Set<String> keySet=doc.keySet();
            for(String key:doc.keySet()){
                if(smbApiId.contains(key)){
                    try{
                        valueHolderF=Float.parseFloat(doc.get(key).toString());
                    }
                    catch (Exception ex) {
                        System.out.println("Catch in smbfaultalertget keyread");
                    }
                    if(!Float.isNaN(valueHolderF)){
                        smbTotCurr=smbTotCurr+valueHolderF;
                    }
                }
            }
            for(String key :keySet){
                String tag=null;
                String error=null;
                String tagkey=null;
                for(int i=0; i<tagArray.length; i++){
                    if(key.contentEquals(tagArray[i][0])){
                        tagkey=tagArray[i][0];
                        tag=tagArray[i][1];
                        if(stringDcCap.size()>i){
                            dcCapacity=stringDcCap.get(i);
                        }
                        else{
                            dcCapacity="0";
                        }
                        error="3000";
                        break;
                    }
                }
                if(tagkey!=null){
                    try{
                        valuHolderF=Float.parseFloat(doc.get(key).toString());
                    }
                    catch(Exception ex){
                        System.out.println("Catch in smb string valuholder");
                    }
                }
                if(!Float.isNaN(valuHolderF)){
                    if(smbTotCurr>1.0 && tagkey!=null && (valuHolderF<=0)){
                        for (String devSQL: devFullName){
                            if(devSQL.contains(doc.getString("device")+";")){
                                record=devSQL.split(";");
                                devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+dcCapacity);
                                genErrCode.add(error);
                                break;
                            }
                        }
                        //continue;
                    }
                }
//                if(tagkey!=null){
//                    for (String devSQL: devFullName){
//                        if(devSQL.contains(doc.getString("device")+";")){
//                            record=devSQL.split(";");
//                            devFullNameAlertClose.add(record[0]+";"+record[1]+"_"+tag+";"+dcCapacity);
//                            error code have to assign
//                            closeErrCode.add(error);
//                            break;
//                        }
//                    }
//                }
            }
        }

        try{
            checkdeviceandgetwithobser.checkDevicesAndProcess(devFullNameAlertGen, deviceTypeRgx, "String", "3000", radiation, minPoaPlant, obserLimit, siteID, siteZone, conn); 
            }
        catch (Exception ex) {
            System.out.println("Catch in smbstringalert checkdeviceandget");
        }

    }
    
}


package com.aws.lambda;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class sqlUpdateAndEntry {
    
    public void sqlEveUpdate(String endDate, String todayD, String eventId, Connection conn) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_events SET end_today= '%s', end_date ='%s', status=2 WHERE id=%s;", todayD, endDate, eventId);
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    //for update in event temp for SMB and string
    public void sqlEveUpdate2(int actCount, String eventId, Connection conn) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET act_count=%s WHERE id=%s;",actCount+1,eventId);
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    //for other device except plant, inverter, smb, string
    public void sqlEveEntry(String deviceName, String plantID, String deviceID, String deviceType, String startDate, String todayD, String errorCode, Connection conn) throws SQLException {
        Statement stmt= conn.createStatement();
        String entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_events (device_name, plant_id, device_id, device_type, start_today, start_date, status, error_code) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, %s)", deviceName, plantID, deviceID, deviceType, todayD, startDate, errorCode);
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    //for other device plant, inverter, smb, string
    public void sqlEveEntry1(String deviceName, String dcCapacity, String plantID, String deviceID, String deviceType, String startDate, String todayD, String errorCode, Connection conn) throws SQLException {
        Statement stmt= conn.createStatement();
        String entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_events (device_name, plant_id, device_id, device_type, start_today, start_date, status, error_code, dc_capacity) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, %s, %s)", deviceName, plantID, deviceID, deviceType, todayD, startDate, errorCode, dcCapacity);
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    //for entry in event temp for SMB and string
    public void sqlEveEntry2(String deviceName, String dcCapacity, String plantID, String deviceID, String deviceType, String startDate, String todayD, String errorCode, Connection conn) throws SQLException {
        Statement stmt= conn.createStatement();
        String entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp (device_name, plant_id, device_id, device_type, dc_capacity, date_time, error_code, act_count) VALUES ('%s', %s, %s, '%s', '%s', '%s', '%s', 1)", deviceName, plantID, deviceID, deviceType, dcCapacity, startDate, errorCode);
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    public void sqlEveDelete(String eventId, Connection conn) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events WHERE id=%s;", eventId);
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    public void sqlEveDelete1(String eventId, Connection conn) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
}
    
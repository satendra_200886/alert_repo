package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

public class trackParamAlert {
    
    checkDeviceAndGet checkdeviceandget= new checkDeviceAndGet();
    
    public List<String> trackFaultAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, int minPoaPlant, String siteID, String siteZone, Connection conn) throws SQLException{
        String errorCode="9000";
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<String> invAlertList= new ArrayList<>();
        List<Document> docListRem=new ArrayList<>();
        for(Document doc:docList){
            try{
                if(Integer.parseInt(doc.get("tr8").toString())==2){
                    invAlertList.add(doc.getString("device"));
                    docListRem.add(doc);
                }
            }
            catch (Exception ex) {
                System.out.println("Catch in adding tracker fault "+doc.getString("device"));
            }
            
        }

        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> invIDList=new ArrayList<>();
        for (String devMongo: invAlertList){
            for (String devSQL: devFullName){
                //for correct matching at once each
                if(devSQL.contains(devMongo+";")){
                    devFullNameAlertGen.add(devSQL);
                    invIDList.add(devMongo.split("_")[2]);
                    break;
                }
            }
        }
        devFullName.removeAll(devFullNameAlertGen);
        docList.removeAll(docListRem);

        try{
           checkdeviceandget.checkDevicesAndProcess(rsEvent, devFullNameAlertGen, deviceTypeRgx, "Tracker", errorCode, radiation, minPoaPlant, siteID, siteZone, conn); 
        }
        catch (Exception ex) {
            System.out.println("Catch in tracker checkdeviceandget");
        }
        return invIDList;
    }
    
    
}
package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.bson.Document;

public class vcbParamAlert {
    checkParamAndGet checkparamandget= new checkParamAndGet();
   
    public void vcbParamAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, String siteID, String siteZone, Connection conn) throws SQLException{
        String vcbVaribAlarmVal= "SELECT an.API_Id, an.API_name, an.Alarm, e.Error_code FROM  `Flexi_solar_VCB_variable_list` AS an JOIN  Flexi_solar_events_variable_list AS e ON an.API_Name= e.Error_desc;";
        Statement stmt= conn.createStatement();
        ResultSet rsVcbVaribAlarmVal=stmt.executeQuery(vcbVaribAlarmVal);
//        String[][] tagArray={{"s11","String-1","3000"}, {"s12","String-2","3001"}, {"s13","String-3","3002"}, {"s14","String-4","3003"}, {"s15","String-5","3004"}, {"s16","String-6","3005"}, {"s17","String-7","3006"}, {"s18","String-8","3007"}, {"s19","String-9","3008"}, {"s20","String-10","3009"}, {"s21","String-11","3010"}, 
//            {"s22","String-12","3011"}, {"s23","String-13","3012"}, {"s24","String-14","3013"}, {"s25","String-15","3014"}, {"s26","String-16","3015"}, {"s27","String-17","3016"}, {"s28","String-18","3017"}, {"s29","String-19","3018"}, {"s30","String-20","3019"}, {"s31","String-21","3020"}, {"s32","String-22","3021"}, {"s33","String-23","3022"}, {"s34","String-24","3023"}};
        String datetime=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> devFullNameAlertClose= new ArrayList<>();
        List<String> genErrCode= new ArrayList<>();
        List<String> closeErrCode= new ArrayList<>();
        for(Document doc:docList){
            Set<String> keySet=doc.keySet();
            for(String key :keySet){
                //String tag=null;
                String tagkey=null;
                String error=null;
                String alarmValue=null;
                
                rsVcbVaribAlarmVal.beforeFirst();
                while(rsVcbVaribAlarmVal.next()){
                //for(int i=0; i<tagArray.length; i++){
                    if(key.contentEquals(rsVcbVaribAlarmVal.getString("API_Id"))){
                        tagkey=rsVcbVaribAlarmVal.getString("API_Id");
                        //tag=rsVcbVaribAlarmVal.getString("API_Name");
                        error=rsVcbVaribAlarmVal.getString("Error_code");
                        alarmValue=rsVcbVaribAlarmVal.getString("Alarm");
                        break;
                    }
                }
                if(tagkey!=null && ((doc.get(key).toString()).contentEquals(alarmValue))){
                    for (String devSQL: devFullName){
                        if(devSQL.contains(doc.getString("device")+";")){
                            devFullNameAlertGen.add(devSQL);
                            genErrCode.add(error);
                            break;
                        }
                    }
                    continue;
                }
                if(tagkey!=null){
                    for (String devSQL: devFullName){
                        if(devSQL.contains(doc.getString("device")+";")){
                            devFullNameAlertClose.add(devSQL);
                            closeErrCode.add(error);
                            break;
                        }
                    }
                }
            }
        }
        
        try{
            checkparamandget.checkDevicesAndProcess(rsEvent, devFullNameAlertGen, deviceTypeRgx, "VCB", genErrCode, siteID, siteZone, conn); 
        }
        catch (Exception ex) {
            System.out.println("Catch in vcb checkdeviceandget");
        }
    }
}

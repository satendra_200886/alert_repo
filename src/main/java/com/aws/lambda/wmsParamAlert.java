
package com.aws.lambda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.bson.Document;


public class wmsParamAlert {
    checkDeviceAndGet checkdeviceandget= new checkDeviceAndGet();
    
    public void wmsSensorAlert(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, String siteID, String siteZone, Connection conn) throws SQLException, ParseException{
        String[] record=new String[3];
        String deviceType="WMS Sensor";
        float valuHolderF=0.0f;
        String errorCode="8000";
        List<Float> medianValue=new ArrayList<>();
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        String datetime=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        
        medianValue=poaGhiModAmbTempMedian(docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn);
        String[][] tagArray={{"w21","POA-1"}, {"w22","POA-2"}, {"w32","GHI-1"}, {"w10","Mod-Temp1"}, {"w14","Mod-Temp2"}, {"w13","Amb-Temp"}};
        
        try{
            case1(datetime, todayD, tagArray, medianValue, docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn);
        }
        catch(Exception ex){
            System.out.println("Catch in wms sesor case1");
        }
        
        try{
            case2(datetime, todayD, tagArray, medianValue, docList, devFullName, rsEvent, deviceTypeRgx, radiation, siteID, siteZone, conn);
        }
        catch(Exception ex){
            System.out.println("Catch in wms sesor case2");
        }
        
    }
    
    
    public void case1(String datetime, String todayD, String[][] tagArray, List<Float> medianValue, List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, String siteID, String siteZone, Connection conn) throws SQLException, ParseException{
        List<String> devFullNameAlertGen= new ArrayList<>();
        String[] record=new String[3];
        String deviceType="WMS Sensor";
        float valuHolderF=0.0f;
        String errorCode="8000";
        
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDTMinus=zoneddatetime.minusMinutes(5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        Statement stmt1= conn.createStatement();
        String invDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily where plant_id=%s and Today=%s and AC_Power>0 order by last_data_time desc;", siteID, todayD);  
        ResultSet invDailyRs=stmt1.executeQuery(invDailyQ);

        if(invDailyRs.next()){
            if(dateFormat.parse(invDailyRs.getString("last_data_time")).compareTo(dateFormat.parse(currDTMinus))<0){
                return;
            }
        }
        else{
            return;
        }
        
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        Statement stmt4= conn.createStatement();
        String eventDetTemp=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp where plant_id=%s and device_type='%s' and error_code=%s and main_entry=0 and case_cond=1;", siteID, deviceType, errorCode);
        String eventDetTempMain=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp where plant_id=%s and device_type='%s' and error_code=%s and main_entry=1 and case_cond=1;", siteID, deviceType, errorCode);
        String query=null;
        ResultSet rsEventTemp= stmt2.executeQuery(eventDetTemp);
        ResultSet rsEventTempMain= stmt3.executeQuery(eventDetTempMain);
        
        for(Document doc:docList){
            Set<String> keySet=doc.keySet();
            for(String key :keySet){
                String tag=null;
                String tagkey=null;
                for(int i=0; i<tagArray.length; i++){
                    if(key.contentEquals(tagArray[i][0])){
                        tagkey=tagArray[i][0];
                        tag=tagArray[i][1];
                        break;
                    }
                }
                if(tagkey!=null){
                    try{
                        valuHolderF=Float.parseFloat(doc.get(key).toString());
                    }
                    catch(Exception ex){
                        System.out.println("Catch in wms sesor valuholder");
                    }
                }
                if(key.contains("w21") || key.contains("w22")){
                    if(docList.size()>0 && medianValue.get(0)>0){
                        if(tagkey!=null && (Float.isNaN(valuHolderF) || valuHolderF<0.01)){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(key.contains("w32")){
                    if(docList.size()>0 && medianValue.get(1)>0){
                        if(tagkey!=null && (Float.isNaN(valuHolderF) || valuHolderF<0.01)){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(key.contains("w10") || key.contains("w14")){
                    if(docList.size()>0 && medianValue.get(2)>0){
                        if(tagkey!=null && (Float.isNaN(valuHolderF) || valuHolderF<0.01)){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(key.contains("w13")){
                    if(docList.size()>0 && medianValue.get(3)>0){
                        if(tagkey!=null && (Float.isNaN(valuHolderF) || valuHolderF<0.01)){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        //for entry in temp and main
        String eventId;
        String deviceID;
        String deviceName;
        int totCount=0;
        int actCount=0;
        boolean match;
        rsEventTemp.beforeFirst();
        while(rsEventTemp.next()){
            match=false;
            eventId=rsEventTemp.getString("id");
            deviceID=rsEventTemp.getString("device_id");
            deviceName=rsEventTemp.getString("device_name");
            totCount=Integer.parseInt(rsEventTemp.getString("tot_count"));
            actCount=Integer.parseInt(rsEventTemp.getString("act_count"));
            //if we need to remove the event temp entry
            if(totCount==14 && actCount<=6){
                query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                stmt4.execute(query);
                //for not execute next codes as the entry have been deleted
                continue;
            }
            for(String dev:devFullNameAlertGen){
                if(dev.contains(siteID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                    match=true;
                    break;
                }
            }
            if(match){
                if(actCount==7){
                    query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, main_entry=1 WHERE id=%s;",15,actCount+1,eventId);
                    stmt4.execute(query);
                    rsEvent.beforeFirst();
                    while(rsEvent.next()){
                        if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                            break;
                        }
                    }
                    query=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_events (device_name, plant_id, device_id, device_type, start_today, start_date, status, error_code) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, %s)", deviceName, siteID, deviceID, deviceType, new SimpleDateFormat("ddMMyyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rsEventTemp.getString("date_time"))), rsEventTemp.getString("date_time"), errorCode);
                    stmt4.execute(query);
                }
                else if(totCount<14 && actCount<=6){
                    query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s WHERE id=%s;",totCount+1,actCount+1,eventId);
                    stmt4.execute(query);
                }
            }
            else{
                query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s WHERE id=%s;",totCount+1,eventId);
                stmt4.execute(query);
            }
        }
        
        //for new entry
        for(String devDown:devFullNameAlertGen){
            match=false;
            rsEventTemp.beforeFirst();
            rsEventTemp.beforeFirst();
            while(rsEventTemp.next()){
                deviceName=rsEventTemp.getString("device_name");
                //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                if(deviceName.contentEquals(devDown.split(";")[1])) {
                    match=true;
                    break;
                }
            }
            if(!match){
                rsEventTempMain.beforeFirst();
                while(rsEventTempMain.next()){
                    deviceName=rsEventTempMain.getString("device_name");
                    //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                    if(deviceName.contentEquals(devDown.split(";")[1])) {
                        match=true;
                        break;
                    }
                }
            }
            
            if(!match){
                query=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp (device_name, plant_id, device_id, device_type, date_time, error_code, tot_count, act_count, main_entry, case_cond) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, 1, 0, 1)", devDown.split(";")[1], siteID, devDown.split(";")[0].split("_")[2], deviceType, datetime, errorCode);
                stmt4.execute(query);
            }
        }
        
        //for closing of main
        rsEventTempMain.beforeFirst();
        while(rsEventTempMain.next()){
            match=false;
            eventId=rsEventTempMain.getString("id");
            deviceID=rsEventTempMain.getString("device_id");
            deviceName=rsEventTempMain.getString("device_name");
            totCount=Integer.parseInt(rsEventTempMain.getString("tot_count"));
            actCount=Integer.parseInt(rsEventTempMain.getString("act_count"));
            if(totCount==1 && actCount>1){
                //time updated again for counter
                query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s,act_count=%s,date_time='%s' WHERE id=%s;",15,8,datetime,eventId);
                stmt4.execute(query);
                //for not execute next codes as the entry have been deleted
                continue;
            }
            for(String dev:devFullNameAlertGen){
                if(dev.contains(siteID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                    match=true;
                    break;
                }
            }
            if(!match){
                if(actCount==1){
                    query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                    stmt4.execute(query);
                    rsEvent.beforeFirst();
                    while(rsEvent.next()){
                        if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_events SET end_today= '%s', end_date ='%s', status=2 WHERE id=%s;", todayD, datetime, rsEvent.getString("id"));
                            stmt4.execute(query);
                            break;
                        }
                    }
                }
                else if(totCount>1 && actCount>1){
                    query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s WHERE id=%s;",totCount-1,actCount-1,eventId);
                    stmt4.execute(query);
                }
            }
            else{
                query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s WHERE id=%s;",totCount-1,eventId);
                stmt4.execute(query);
            }
        }
        
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
            stmt4.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    
    public void case2(String datetime, String todayD, String[][] tagArray, List<Float> medianValue, List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, String siteID, String siteZone, Connection conn) throws SQLException, ParseException{
        List<String> devFullNameAlertGen= new ArrayList<>();
        List<String> wmsPoaList=new ArrayList<>();
        String[] record=new String[3];
        String deviceType="WMS Sensor";
        float valueHolderF=0.0f;
        String valueHolderS=null;
        String errorCode="8001";
        float poa1=0.0f;
        float poa2=0.0f;
        float poa=0.0f;
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        String eventDetTemp=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp where plant_id=%s and device_type='%s' and error_code=%s and main_entry=0 and case_cond=2;", siteID, deviceType, errorCode);
        String eventDetTempMain=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp where plant_id=%s and device_type='%s' and error_code=%s and main_entry=1 and case_cond=2;", siteID, deviceType, errorCode);
        String query=null;
        ResultSet rsEventTemp= stmt1.executeQuery(eventDetTemp);
        ResultSet rsEventTempMain= stmt2.executeQuery(eventDetTempMain);
        
        for(Document doc:docList){
            Set<String> keySet=doc.keySet();
            try{
                if(keySet.contains("w21")){
                    valueHolderF=Float.parseFloat(doc.get("w21").toString());
                    if(!Float.isNaN(valueHolderF)){
                        poa1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w21");
            }
            try{
                if(keySet.contains("w22")){
                    valueHolderF=Float.parseFloat(doc.get("w22").toString());
                    if(!Float.isNaN(valueHolderF)){
                        poa2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w22");
            }
            
            wmsPoaList.add(doc.getString("device")+";"+poa1+";"+poa2);
            
            for(String key :keySet){
                String tag=null;
                String tagkey=null;
                for(int i=0; i<tagArray.length; i++){
                    if(key.contentEquals(tagArray[i][0])){
                        tagkey=tagArray[i][0];
                        tag=tagArray[i][1];
                        if(key.contentEquals("w21")){
                            poa=poa1;
                        }
                        else if(key.contentEquals("w22")){
                            poa=poa2;
                        }
                        else{
                            if(poa1>0){
                                poa=poa1;
                                if(poa2>0){
                                    poa=(poa1+poa2)/2;
                                }
                            }
                            else{
                                poa=poa2;
                            }
                        }
                        break;
                    }
                }
                if(tagkey!=null){
                    try{
                        valueHolderF=Float.parseFloat(doc.get(key).toString());
                    }
                    catch(Exception ex){
                        System.out.println("Catch in wms sensor valuholder");
                    }
                }
                if(Float.isNaN(valueHolderF)){
                    continue;
                }
                
                if(key.contains("w21") || key.contains("w22")){
                    if(docList.size()>0 && medianValue.get(0)>=350){
                        if(tagkey!=null && Math.abs(valueHolderF-medianValue.get(0))>20){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+poa);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(key.contains("w32")){
                    if(docList.size()>0 && medianValue.get(1)>=350){
                        if(tagkey!=null && Math.abs(valueHolderF-medianValue.get(1))>20){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+poa);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }   
                    }
                }
                else if(key.contains("w10") || key.contains("w14")){
                    if(docList.size()>0 && medianValue.get(2)>0){
                        if(tagkey!=null && Math.abs(valueHolderF-medianValue.get(2))>3){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+poa);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(key.contains("w13")){
                    if(docList.size()>0 && medianValue.get(3)>0){
                        if(tagkey!=null && Math.abs(valueHolderF-medianValue.get(3))>3){
                            for (String devSQL: devFullName){
                                if(devSQL.contains(doc.getString("device")+";")){
                                    record=devSQL.split(";");
                                    devFullNameAlertGen.add(record[0]+";"+record[1]+"_"+tag+";"+poa);
                                    //genErrCode.add(error);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        //for entry in temp and main
        String eventId;
        String deviceID;
        String deviceName;
        float totCount=0.0f;
        float actCount=0.0f;
        float poaLastMin=0.0f;
        float poaRateCount=0.0f;
        String deviceFullNameMatch=null;
        boolean match;
        while(rsEventTemp.next()){
            match=false;
            eventId=rsEventTemp.getString("id");
            deviceID=rsEventTemp.getString("device_id");
            deviceName=rsEventTemp.getString("device_name");
            totCount=Integer.parseInt(rsEventTemp.getString("tot_count"));
            actCount=Integer.parseInt(rsEventTemp.getString("act_count"));
            poaRateCount=Integer.parseInt(rsEventTemp.getString("poa_rate_change"));
            poaLastMin=Float.parseFloat(rsEventTemp.getString("poa_inst"));
            //if we need to remove the event temp entry
            if(deviceName.contains("POA") || deviceName.contains("GHI")){
                if((totCount==119 && actCount<=101) || poaRateCount>48){
                    query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                    stmt3.execute(query);
                    //for not execute next codes as the entry have been deleted
                    continue;
                }
            }
            else if(deviceName.contains("Temp")){
                if((totCount==59 && actCount<=50) || poaRateCount>24){
                    query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                    stmt3.execute(query);
                    //for not execute next codes as the entry have been deleted
                    continue;
                }
            }

            for(String dev:devFullNameAlertGen){
                if(dev.contains(siteID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                    match=true;
                    deviceFullNameMatch=dev;
                    break;
                }
            }
            if(match){
                if(deviceFullNameMatch.contains("POA") || deviceFullNameMatch.contains("GHI")){
                    if(actCount==101 && (poaRateCount<48 || (poaRateCount==48 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))<20))){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, main_entry=1, poa_rate_change=48, poa_inst=%s  WHERE id=%s;",120,actCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                        rsEvent.beforeFirst();
                        while(rsEvent.next()){
                            if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                                break;
                            }
                        }
                        query=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_events (device_name, plant_id, device_id, device_type, start_today, start_date, status, error_code) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, %s)", deviceName, siteID, deviceID, deviceType, new SimpleDateFormat("ddMMyyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rsEventTemp.getString("date_time"))), rsEventTemp.getString("date_time"), errorCode);
                        stmt3.execute(query);
                    }
                    else if(totCount<119 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount+1,actCount+1,poaRateCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                    }
                    else if(totCount<119 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))<20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount+1,actCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                    }
                }
                else if(deviceFullNameMatch.contains("Temp")){
                    if(actCount==50 && (poaRateCount<24 || (poaRateCount==24 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))<20))){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, main_entry=1, poa_rate_change=48, poa_inst=%s  WHERE id=%s;",60,actCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                        rsEvent.beforeFirst();
                        while(rsEvent.next()){
                            if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                                break;
                            }
                        }
                        query=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_events (device_name, plant_id, device_id, device_type, start_today, start_date, status, error_code) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, %s)", deviceName, siteID, deviceID, deviceType, new SimpleDateFormat("ddMMyyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rsEventTemp.getString("date_time"))), rsEventTemp.getString("date_time"), errorCode);
                        stmt3.execute(query);
                    }
                    else if(totCount<59 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount+1,actCount+1,poaRateCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                    }
                    else if(totCount<59 && Math.abs(poaLastMin-Float.parseFloat(deviceFullNameMatch.split(";")[2]))<20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount+1,actCount+1,deviceFullNameMatch.split(";")[2],eventId);
                        stmt3.execute(query);
                    }
                }
            }
            else{
                for(String s:wmsPoaList){
                    if(s.contains(siteID+"_WMS_"+deviceID)){
                        if(deviceName.contains("POA-1")){
                            poa1=Float.parseFloat(s.split(";")[1]);
                        }
                        else if(deviceName.contains("POA-2")){
                            poa2=Float.parseFloat(s.split(";")[2]);
                        }
                    }
                }
                if(deviceName.contains("POA") && medianValue.get(0)>=350){
                    if(deviceName.contains("POA-1")){
                        poa=poa1;
                    }
                    else{
                        poa=poa2;
                    }
                    if(Math.abs(poaLastMin-poa)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount+1,poaRateCount+1,poa,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount+1,poa,eventId);
                    }
                    if(query!=null){
                        stmt3.execute(query);
                    }
                }
                else if(deviceName.contains("GHI") && medianValue.get(1)>=350){
                    if(poa1>0){
                        poa=poa1;
                        if(poa2>0){
                            poa=(poa1+poa2)/2;
                        }
                    }
                    else{
                        poa=poa2;
                    }
                    if(Math.abs(poaLastMin-poa)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount+1,poaRateCount+1,poa,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount+1,poa,eventId);
                    }
                    if(query!=null){
                        stmt3.execute(query);
                    }
                }
                else if(deviceName.contains("Temp")){
                    if(poa1>0){
                        poa=poa1;
                        if(poa2>0){
                            poa=(poa1+poa2)/2;
                        }
                    }
                    else{
                        poa=poa2;
                    }
                    if(Math.abs(poaLastMin-poa)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount+1,poaRateCount+1,poa,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount+1,poa,eventId);
                    }
                    if(query!=null){
                        stmt3.execute(query);
                    }
                }
            }
        }

        //for new entry
        for(String devDown:devFullNameAlertGen){
            match=false;
            rsEventTemp.beforeFirst();
            while(rsEventTemp.next()){
                deviceName=rsEventTemp.getString("device_name");
                //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                if(deviceName.contentEquals(devDown.split(";")[1])) {
                    match=true;
                    break;
                }
            }
            if(!match){
                rsEventTempMain.beforeFirst();
                while(rsEventTempMain.next()){
                    deviceName=rsEventTempMain.getString("device_name");
                    //loop all event, matching for, if entry is already available in event for same and dont have to do anaything, making match false
                    if(deviceName.contentEquals(devDown.split(";")[1])) {
                        match=true;
                        break;
                    }
                }
            }
            
            if(!match){
                query=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp (device_name, plant_id, device_id, device_type, date_time, error_code, tot_count, act_count, main_entry, case_cond, poa_inst, poa_rate_change) VALUES ('%s', %s, %s, '%s', '%s', '%s', 1, 1, 0, 2, %s, %s);", devDown.split(";")[1], siteID, devDown.split(";")[0].split("_")[2], deviceType, datetime, errorCode, devDown.split(";")[2], 1);
                stmt3.execute(query);
            }
        }

        //for closing of main
        while(rsEventTempMain.next()){
            match=false;
            eventId=rsEventTempMain.getString("id");
            deviceID=rsEventTempMain.getString("device_id");
            deviceName=rsEventTempMain.getString("device_name");
            totCount=Integer.parseInt(rsEventTempMain.getString("tot_count"));
            actCount=Integer.parseInt(rsEventTempMain.getString("act_count"));
            poaRateCount=Integer.parseInt(rsEventTempMain.getString("poa_rate_change"));
            poaLastMin=Float.parseFloat(rsEventTempMain.getString("poa_inst"));
            //resetting in temp to close main at next matching 
            for(String s:wmsPoaList){
                if(s.contains(siteID+"_WMS_"+deviceID)){
                    if(deviceName.contains("POA-1")){
                        poa1=Float.parseFloat(s.split(";")[1]);
                    }
                    else if(deviceName.contains("POA-2")){
                        poa2=Float.parseFloat(s.split(";")[2]);
                    }
                }
            }
            if((totCount==1 && actCount>1) || poaRateCount==1){
                if(deviceName.contains("POA")){
                    if(deviceName.contains("POA-1")){
                        poa=poa1;
                    }
                    else{
                        poa=poa2;
                    }
                    if(poaRateCount>1 || Math.abs(poa-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=48, poa_inst=%s, date_time='%s'  WHERE id=%s;",120,102,poa,datetime,eventId);
                    }
                }
                if(deviceName.contains("GHI")){
                    if(poa1>0){
                        poa=poa1;
                        if(poa2>0){
                            poa=(poa1+poa2)/2;
                        }
                    }
                    else{
                        poa=poa2;
                    }
                    if(poaRateCount>1 || Math.abs(poa-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=48, poa_inst=%s, date_time='%s'  WHERE id=%s;",120,102,poa,datetime,eventId);
                    }
                }
                if(deviceName.contains("Temp")){
                    if(poa1>0){
                        poa=poa1;
                        if(poa2>0){
                            poa=(poa1+poa2)/2;
                        }
                    }
                    else{
                        poa=poa2;
                    }
                    if(poaRateCount>1 || Math.abs(poa-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=48, poa_inst=%s, date_time='%s'  WHERE id=%s;",60,51,poa,datetime,eventId);
                    }
                }
                if(query!=null){
                    stmt3.execute(query);
                }
                //for not execute next codes as the entry have been deleted
                continue;
            }

            for(String dev:devFullNameAlertGen){
                if(dev.contains(siteID+"_"+deviceTypeRgx+"_"+deviceID+";"+deviceName)){
                    deviceFullNameMatch=dev;
                    match=true;
                    break;
                }
            }

            if(!match){
                if(actCount==1 && poaRateCount>1){
                    if(deviceName.contains("POA") || deviceName.contains("GHI")){
                        if(deviceName.contains("POA")&& medianValue.get(0)>350){
                            query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                            stmt3.execute(query);
                            rsEvent.beforeFirst();
                            while(rsEvent.next()){
                                if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                                    query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_events SET end_today= '%s', end_date ='%s', status=2 WHERE id=%s;", todayD, datetime, rsEvent.getString("id"));
                                    stmt3.execute(query);
                                    break;
                                }
                            }
                        }
                        else if(deviceName.contains("GHI")&& medianValue.get(1)>350){
                            query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                            stmt3.execute(query);
                            rsEvent.beforeFirst();
                            while(rsEvent.next()){
                                if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                                    query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_events SET end_today= '%s', end_date ='%s', status=2 WHERE id=%s;", todayD, datetime, rsEvent.getString("id"));
                                    stmt3.execute(query);
                                    break;
                                }
                            }
                        }
                    }
                    else if(deviceName.contains("Temp")){
                        query=String.format("DELETE FROM swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp WHERE id=%s;", eventId);
                        stmt3.execute(query);
                        rsEvent.beforeFirst();
                        while(rsEvent.next()){
                            if(rsEvent.getString("device_id").contentEquals(deviceID) && rsEvent.getString("device_type").contentEquals(deviceType) && rsEvent.getString("error_code").contentEquals(errorCode) && rsEvent.getString("device_name").contentEquals(deviceName)){
                                query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_events SET end_today= '%s', end_date ='%s', status=2 WHERE id=%s;", todayD, datetime, rsEvent.getString("id"));
                                stmt3.execute(query);
                                break;
                            }
                        }
                    }
                }
                else if(totCount>1 && actCount>1 && poaRateCount>1){
                    if(deviceName.contains("POA-1") && medianValue.get(0)>350){
                        if(Math.abs(poa1-poaLastMin)>20){
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poaLastMin+1,poa1,eventId);
                        }
                        else{
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poa1,eventId);
                        }
                    }
                    else if(deviceName.contains("POA-2") && medianValue.get(0)>350){
                        if(Math.abs(poa2-poaLastMin)>20){
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poaLastMin+1,poa2,eventId);
                        }
                        else{
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poa2,eventId);
                        }
                    }
                    else if(deviceName.contains("GHI") && medianValue.get(1)>350){
                        if(Math.abs(poa-poaLastMin)>20){
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poaLastMin+1,poa,eventId);
                        }
                        else{
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poa,eventId);
                        }
                    }
                    else if(deviceName.contains("Temp")){
                        if(Math.abs(poa-poaLastMin)>20){
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poaLastMin+1,poa,eventId);
                        }
                        else{
                            query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, act_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,actCount-1,poa,eventId);
                        }
                    }
                    if(query!=null){
                        stmt3.execute(query);
                    }
                }
            }
            else{
                if(deviceName.contains("POA-1") && medianValue.get(0)>350){
                    if(Math.abs(poa1-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,poaLastMin+1,poa1,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,poa1,eventId);
                    }
                }
                else if(deviceName.contains("POA-2") && medianValue.get(0)>350){
                    if(Math.abs(poa2-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,poaLastMin+1,poa2,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,poa2,eventId);
                    }
                }
                else if(deviceName.contains("GHI") && medianValue.get(1)>350){
                    if(Math.abs(poa-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,poaLastMin+1,poa,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,poa,eventId);
                    }
                }
                else if(deviceName.contains("Temp")){
                    if(Math.abs(poa-poaLastMin)>20){
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_rate_change=%s, poa_inst=%s WHERE id=%s;",totCount-1,poaLastMin+1,poa,eventId);
                    }
                    else{
                        query=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_event_temp SET tot_count=%s, poa_inst=%s WHERE id=%s;",totCount-1,poa,eventId);
                    }
                }
                if(query!=null){
                    stmt3.execute(query);
                }
            }
        }
        
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    
    public List<Float> poaGhiModAmbTempMedian(List<Document> docList, List<String> devFullName, ResultSet rsEvent, String deviceTypeRgx, float radiation, String siteID, String siteZone, Connection conn){
        List<Float> returnList=new ArrayList<>();
        float poaMedian=0.0f;
        float ghiMedian=0.0f;
        float modTempMedian=0.0f;
        float ambTempMedian=0.0f;
        List<Float> poaList=new ArrayList<>();
        List<Float> ghiList=new ArrayList<>();
        List<Float> modTempList=new ArrayList<>();
        List<Float> ambTempList=new ArrayList<>();
        float valueHolderF=0.0f;
        Set keyset;
        
        for(Document docLatest:docList){
            keyset=docLatest.keySet();

            try{
                if(keyset.contains("w21")){
                    valueHolderF=Float.parseFloat(docLatest.get("w21").toString());
                    if(!Float.isNaN(valueHolderF)){
                        poaList.add(valueHolderF);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w21");
            }
            try{
                if(keyset.contains("w22")){
                    valueHolderF=Float.parseFloat(docLatest.get("w22").toString());
                    if(!Float.isNaN(valueHolderF)){
                        poaList.add(valueHolderF);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w22");
            }
            
            try{
                if(keyset.contains("w32")){
                    valueHolderF=Float.parseFloat(docLatest.get("w32").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ghiList.add(valueHolderF);
                    }
                    
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w32");
            }
            try{
                if(keyset.contains("w13")){
                    valueHolderF=Float.parseFloat(docLatest.get("w13").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ambTempList.add(valueHolderF);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w13");
            }
            try{
                if(keyset.contains("w10")){
                    valueHolderF=Float.parseFloat(docLatest.get("w10").toString());
                    if(!Float.isNaN(valueHolderF)){
                        modTempList.add(valueHolderF);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w10");
            }
            try{
                if(keyset.contains("w14")){
                    valueHolderF=Float.parseFloat(docLatest.get("w14").toString());
                    if(!Float.isNaN(valueHolderF)){
                        modTempList.add(valueHolderF);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w14");
            }
        }
        
        float[] poaArray = new float[poaList.size()];
        float[] ghiArray = new float[ghiList.size()];
        float[] modTempArray = new float[modTempList.size()];
        float[] ambTempArray = new float[ambTempList.size()];

        int index = 0;
        for (final Float value: poaList) {
           poaArray[index++] = value;
        }
        index = 0;
        for (final Float value: ghiList) {
           ghiArray[index++] = value;
        }
        index = 0;
        for (final Float value: modTempList) {
           modTempArray[index++] = value;
        }
        index = 0;
        for (final Float value: ambTempList) {
           ambTempArray[index++] = value;
        }

        Arrays.sort(poaArray);
        Arrays.sort(ghiArray);
        Arrays.sort(modTempArray);
        Arrays.sort(ambTempArray);
        
        poaMedian=median(poaArray);
        ghiMedian=median(ghiArray);
        modTempMedian=median(modTempArray);
        ambTempMedian=median(ambTempArray);
        
        returnList.add(poaMedian);
        returnList.add(ghiMedian);
        returnList.add(modTempMedian);
        returnList.add(ambTempMedian);
        
        return returnList;
    }
    
    
    public float median(float[] floatArray){
        float median=0.0f;
        if(floatArray.length%2==0){
            median=(floatArray[(floatArray.length/2)-1]+floatArray[floatArray.length/2])/2;
        }
        else{
            median=floatArray[(int)((floatArray.length/2)-0.5)];
        }
        
        return median;
    }
    
    
}
